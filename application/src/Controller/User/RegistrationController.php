<?php

namespace App\Controller\User;

use App\Controller\BaseController;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Lib\User\UserField;
use App\Security\ApplicationAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends BaseController
{
    private User $user;
    private FormInterface $form;
    private GuardAuthenticatorHandler $guardHandler;
    private ApplicationAuthenticator $authenticator;
    private UserPasswordEncoderInterface $passwordEncoder;

    private RedirectResponse $DEFAULT_REDIRECT_RESPONSE;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, ApplicationAuthenticator $authenticator, GuardAuthenticatorHandler $guardHandler)
    {
        $this->user = new User();
        $this->guardHandler = $guardHandler;
        $this->authenticator = $authenticator;
        $this->passwordEncoder = $passwordEncoder;
        $this->DEFAULT_REDIRECT_RESPONSE = new RedirectResponse('/');
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request): Response
    {
        if ($this->isUserAuth())
        {
            return $this->DEFAULT_REDIRECT_RESPONSE;
        }
        $this->form = $this->createForm(RegistrationFormType::class, $this->user);
        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $this->SaveNewUser();
            return $this->guardHandler->authenticateUserAndHandleSuccess(
                $this->user,
                $request,
                $this->authenticator,
                'main' // firewall name in security.yaml
            ) ?: $this->DEFAULT_REDIRECT_RESPONSE;
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $this->form->createView(),
        ]);
    }

    private function SaveNewUser()
    {
        $this->PrepareNewUser();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($this->user);
        $entityManager->flush();
    }

    private function PrepareNewUser()
    {
        $this->user->setPassword(
            $this->passwordEncoder->encodePassword(
                $this->user,
                $this->form->get('plainPassword')->getData()
            )
        );
        $this->user->setFirstName($this->form->get(UserField::FIRST_NAME)->getData());
        $this->user->setLastName($this->form->get(UserField::LAST_NAME)->getData());
    }
}
