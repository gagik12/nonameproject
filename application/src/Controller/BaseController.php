<?php

namespace App\Controller;

use App\Entity\User;
use Couchbase\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseController extends AbstractController
{
    protected function getLoggedUser(): ?User
    {
        return $this->getUser();
    }

    protected function isUserAuth()
    {
        return !is_null($this->getLoggedUser());
    }

    /**
     * @param array $data
     * @param string $key
     * @throws \Exception
     */
    protected function tryToGetJsonData(array $data, string $key)
    {
        if (!array_key_exists($key, $data))
        {
            throw new \Exception("Key not exist in array " . $key);
        }

        return $data[$key];
    }
}