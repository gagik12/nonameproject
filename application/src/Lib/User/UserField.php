<?php

namespace App\Lib\User;

class UserField
{
    public const FIRST_NAME = "firstName";
    public const LAST_NAME = "lastName";
}