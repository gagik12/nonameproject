<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3000, nullable=false)
     */
    private $text;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $points;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="questions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private User $user;

    /**
     * @var QuestionSubject
     * @ORM\ManyToOne(targetEntity="App\Entity\QuestionSubject", inversedBy="questions")
     * @ORM\JoinColumn(name="question_subject_id", referencedColumnName="id", nullable=false)
     */
    private QuestionSubject $question_subject;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $created_at;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $modified_at;

    public function __construct() {
        $this->setCreatedAt(new \DateTime()); //TODO: проверить корректность проставления полей created&modified at
        if ($this->getModifiedAt() == null) {
            $this->setModifiedAt(new \DateTime());
        }
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->created_at = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedAt(): \DateTime
    {
        return $this->modified_at;
    }

    /**
     * @param \DateTime $modifiedAt
     */
    public function setModifiedAt(\DateTime $modifiedAt): void
    {
        $this->modified_at = $modifiedAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getQuestionSubject(): ?QuestionSubject
    {
        return $this->question_subject;
    }

    public function setQuestionSubject(QuestionSubject $subject)
    {
        $this->question_subject = $subject;
    }

    public function setPoints(int $points)
    {
        $this->points = $points;
    }

    public function getPoints()
    {
        return $this->points;
    }
}
